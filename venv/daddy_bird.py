import os
import pygame
import random
from pygame.locals import *


def load_image(filename):
    file = os.path.join("images", filename)
    try:
        _image = pygame.image.load(file)
        if _image.get_alpha() is None:
            _image = _image.convert()
        else:
            _image = _image.convert_alpha()
    except pygame.error:
        print("Unable to load file: ", filename)
        os.exit(1)

    return _image, _image.get_rect()


class Game:
    isDone = False

    def reset(background, font, screen):
        points = 0
        text_to_render = "Points: " + str(points)
        text = font.render(text_to_render, True, (10, 10, 10))
        textpos = text.get_rect()
        textpos.centerx = background.get_rect().centerx
        screen.blit(background, (0, 0))
        screen.blit(text, textpos)
        bird = Bird((100, 200))
        bird_sprite = pygame.sprite.RenderPlain(bird)
        for _ in range(3):
            Pillar()
        return bird, bird_sprite, points, textpos

    def init():
        pygame.init()
        screen = pygame.display.set_mode((1200, 800))
        pygame.display.set_caption("Daddy bird")
        clock = pygame.time.Clock()
        _, bottom_edge_screen = screen.get_size()
        background = pygame.Surface(screen.get_size())
        background = background.convert()
        background.fill((255, 255, 255))
        font = pygame.font.Font(None, 36)
        return background, clock, font, screen

    def clean_up(bird):
        bird.kill()
        for pill in Pillar.pillar_group:
            pill.kill()
        Pillar.interval = 0


class Bird(pygame.sprite.Sprite):
    """Bird class
    funcs: update, calculate_new_position
    attrs:
    dx, dy - velocity
    acceleration, jump_size, fall_speed
    """

    def __init__(self, pos):
        super().__init__()
        self.done = False
        self.image, self.rect = load_image("bird.png")

        self.rect.x, self.rect.y = pos
        self.jump = False
        self.dx, self.dy = 0, 0
        self.acceleration = 1
        self.jump_size = 18
        self.fall_speed = 0

    def update(self):
        new_position = self.calculate_new_position(self.rect)
        self.rect = new_position

        if len(pygame.sprite.spritecollide(self, Pillar.pillar_group, False)) > 0:
            self.done = True
        if self.rect.bottom >= pygame.display.get_surface().get_size()[1] or self.rect.top <= 0:
            self.done = True

    def calculate_new_position(self, rect):
        if self.jump:
            self.jump_size = self.jump_size - self.acceleration
            self.dy = -self.jump_size
            if self.jump_size < 0:
                self.fall_speed = 0
                self.jump = False
                self.jump_size = 18
        else:
            self.fall_speed += self.acceleration
            self.dy = self.fall_speed
        return rect.move(self.dx, self.dy)


class Pillar(pygame.sprite.Sprite):
    """Pillar class
    funcs: reinit, update
    attrs: velocity, interval(between pillars), isUpperPipe,
    pillar_down(complementary pillar)
    """

    velocity = 10
    interval = 0
    pillar_group = pygame.sprite.Group()

    def __init__(self, up=True, x_pos=0, y_pos=0):
        super().__init__()
        self.image, self.rect = load_image("pipe.png")
        self.image = pygame.transform.flip(self.image, False, up)
        self.screen = pygame.display.get_surface()
        self.isUpperPipe = False
        if up:
            self.rect.x, _ = self.screen.get_size()
            self.isUpperPipe = True
            self.rect.y = random.randint(-200, 0)
            self.interval = Pillar.interval
            Pillar.interval += 500
            self.rect.x += self.interval
        else:
            self.rect.bottom = 900+y_pos
            self.rect.x = x_pos
        self.onLeft = False
        Pillar.pillar_group.add(self)
        if up:
            self.pillar_down = Pillar(up=False, x_pos=self.rect.x, y_pos=self.rect.y)

    def reinit(self):
        self.rect.x, _ = self.screen.get_size()
        rand = random.randint(-200, 0)
        if self.isUpperPipe:
            self.rect.y = rand
            self.pillar_down.rect.x = self.rect.x
            self.pillar_down.rect.bottom = 900 + self.rect.y

        self.onLeft = False

    def update(self):
        self.rect = self.rect.move(-self.velocity, 0)

        if self.rect.right < -300:
            self.reinit()


def main():
    background, clock, font, screen = Game.init()

    while not Game.isDone:
        bird, bird_sprite, points, textpos = Game.reset(background, font, screen)

        pygame.display.flip()

        while not bird.done:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == QUIT:
                    Game.isDone = True
                    return
                elif event.type == KEYDOWN:
                    if event.key == K_UP:
                        bird.jump = True
                    if event.key == K_ESCAPE:
                        Game.isDone = True
                        bird.done = True

            screen.blit(background, (0, 0))

            for pillar in Pillar.pillar_group:
                if pillar.rect.right < bird.rect.left and not pillar.onLeft:
                    pillar.onLeft = True
                    points += 0.5

            Pillar.pillar_group.update()
            Pillar.pillar_group.draw(screen)

            text_to_render = "Points: " + str(int(points))
            text = font.render(text_to_render, True, (10, 10, 10))

            bird_sprite.update()
            bird_sprite.draw(screen)
            screen.blit(text, textpos)
            pygame.display.flip()

        Game.clean_up(bird)


if __name__ == "__main__":
    main()
